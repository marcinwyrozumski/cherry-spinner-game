<?php


namespace Cherry\SlotMachine\Core\Pattern\Wallet;

use Cherry\SlotMachine\Core\Model\Money;
use Cherry\SlotMachine\Core\Model\Wallet;

/**
 * Class WalletSubject
 * @package Cherry\SlotMachine\Core\Pattern
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class WalletSubject implements \SplSubject
{
    /**
     * @var Wallet
     */
    protected $wallet;

    /**
     * @var Money;
     */
    protected $money;

    /**
     * @var string
     */
    protected $trigger;

    /**
     * @var bool
     */
    private $isNotified = false;

    /**
     * WalletSubject constructor.
     * @param Wallet $wallet
     * @param string $trigger
     * @param Money $money
     */
    public function __construct(Wallet $wallet, $trigger, Money $money = null)
    {
        $this->wallet = $wallet;
        $this->money = $money;
        $this->trigger = $trigger;
    }

    /**
     * @return boolean
     */
    public function isIsNotified()
    {
        return $this->isNotified;
    }

    /**
     * @param boolean $isNotified
     * @return $this
     */
    public function setIsNotified($isNotified)
    {
        $this->isNotified = $isNotified;
        return $this;
    }
    
    /**
     * @return Wallet
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @return Money
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * @return string
     */
    public function getTrigger()
    {
        return $this->trigger;
    }

    /**
     * @var \SplObserver[]
     */
    private $bonusObservers = array();

    /**
     * @param \SplObserver $observer
     */
    public function attach(\SplObserver $observer)
    {
        $this->bonusObservers[] = $observer;
    }

    /**
     * @param \SplObserver $observer
     */
    public function detach(\SplObserver $observer)
    {
        $key = array_search($observer, $this->bonusObservers, true);
        if ($key) {
            unset($this->bonusObservers[$key]);
        }
    }

    /**
     *
     */
    public function notify()
    {
        foreach ($this->bonusObservers as $observer) {
            $observer->update($this);
        }
    }

}