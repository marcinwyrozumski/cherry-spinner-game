<?php


namespace Cherry\SlotMachine\Core\Pattern\Wallet\Observer;

use Cherry\SlotMachine\Core\Model\Bonus;
use Cherry\SlotMachine\Core\Model\Wallet;
use Cherry\SlotMachine\Core\Pattern\Wallet\WalletSubject;

/**
 * Class AbstractBonusObserver
 * @package Cherry\SlotMachine\Core\Pattern\Wallet\Observer
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
abstract class AbstractBonusObserver implements \SplObserver
{

    /**
     * @param \SplSubject|WalletSubject $subject
     */
    public function update(\SplSubject $subject)
    {
        $wallet = $subject->getWallet();

        if ($this->isBonus($wallet) && $this->checkTrigger($subject)) {
            $calculatedMoney = $wallet
                ->getAssociatedBonus()
                ->getValueOfReward()
                ->calculateReward($subject->getMoney());

            $wallet->addMoney($calculatedMoney);
            $subject->setIsNotified(true);
        }
    }

    /**
     * @return string
     */
    abstract public function getObserverBonusTrigger();

    /**
     * @param Wallet $wallet
     * @return bool
     */
    protected function isBonus(Wallet $wallet)
    {
        return Wallet::ORIGIN_BONUS === $wallet->getOrigin();
    }

    /**
     * @param WalletSubject $subject
     * @return bool
     */
    protected function checkTrigger(WalletSubject $subject)
    {
        $wallet = $subject->getWallet();
        $bonus = $wallet->getAssociatedBonus();

        return
            $bonus instanceof Bonus &&
            $this->getObserverBonusTrigger() === $bonus->getTrigger() &&
            $this->getObserverBonusTrigger() === $subject->getTrigger();
    }
}