<?php


namespace Cherry\SlotMachine\Core\Pattern\Wallet\Observer;

use Cherry\SlotMachine\Core\Model\Wallet;
use Cherry\SlotMachine\Core\Pattern\Wallet\WalletSubject;

/**
 * Class RealObserver
 * @package Cherry\SlotMachine\Core\Pattern\Wallet\Observer
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class RealObserver implements \SplObserver
{
    /**
     * @param \SplSubject|WalletSubject $subject
     */
    public function update(\SplSubject $subject)
    {
        if ($subject->getMoney()) {
            $wallet = $subject->getWallet();

            if ($wallet->getOrigin() === Wallet::ORIGIN_DEFAULT) {
                $wallet->addMoney($subject->getMoney());
                
                $subject->setIsNotified(true);
            }
        }
    }

}