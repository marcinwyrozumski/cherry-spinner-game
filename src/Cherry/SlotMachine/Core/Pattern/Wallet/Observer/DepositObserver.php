<?php

namespace Cherry\SlotMachine\Core\Pattern\Wallet\Observer;

use Cherry\SlotMachine\Core\Model\Bonus;

/**
 * Class DepositObserver
 * @package Cherry\SlotMachine\Core\Pattern\Wallet\Observer
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class DepositObserver extends AbstractBonusObserver
{
    
    /** @inheritdoc */
    public function getObserverBonusTrigger()
    {
        return Bonus::TRIGGER_DEPOSIT;
    }


}