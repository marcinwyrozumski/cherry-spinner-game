<?php


namespace Cherry\SlotMachine\Core\Shared;


use Cherry\SlotMachine\Core\Model\Money;

interface WalletInterface
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DEPLETED = 'depleted';

    const ORIGIN_DEFAULT = 'default';
    const ORIGIN_BONUS = 'bonus';
    
    /**
     * @return float
     */
    public function getAmount();
    /**
     * @param Money $money
     * @return $this
     */
    public function addMoney(Money $money);
    /**
     * @param Money $money
     * @return $this
     */
    public function removeMoney(Money $money);
}