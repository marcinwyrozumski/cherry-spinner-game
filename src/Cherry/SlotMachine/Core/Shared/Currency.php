<?php


namespace Cherry\SlotMachine\Core\Shared;

/**
 * Class Currency
 * @package Cherry\SlotMachine\Model
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Currency
{
    const EUR = 'EUR';
}