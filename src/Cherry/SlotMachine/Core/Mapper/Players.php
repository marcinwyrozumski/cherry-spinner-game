<?php


namespace Cherry\SlotMachine\Core\Mapper;

use Cherry\SlotMachine\Core\Gateway\Players as PlayersGateway;
use Cherry\SlotMachine\Core\Model\Player;
use Cherry\SlotMachine\Core\Model\Wallet;

/**
 * Class Players
 * @package Cherry\SlotMachine\Core\Mapper
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Players extends AbstractMapper
{
    /**
     * @return PlayersGateway
     */
    public function getGateway()
    {
        return $this->app['gateways.players'];
    }

    /**
     * @param Player $model
     * @param array $data
     * @return Player
     */
    public function populate($model, $data)
    {
        /** @var Player $model */
        $model = parent::populate($model, $data);

        // populate real wallet
        if (!empty($data['realWallet'])) {
            /** @var Wallet $wallet */
            $wallet = $this->getWalletsMapper()->findOne($data['realWallet']);
            $model->setRealWallet($wallet);
        }

        // populate bonus wallets
        if (!empty($data['bonusWallets']) && is_array($data['bonusWallets'])) {
            $wallets = [];

            foreach ($data['bonusWallets'] as $walletId) {
                $wallets[] = $this->getWalletsMapper()->findOne($walletId);
            }

            $model->setBonusWallets($wallets);
        }

        return $model;
    }

    /**
     * @param Player $model
     * @return array
     */
    public function toArray($model)
    {
        $array = parent::toArray($model);

        if ($model->getRealWallet()) {
            $array['realWallet'] = $model->getRealWallet()->getId();
        }

        if ($model->getBonusWallets()) {
            $array['bonusWallets'] = [];

            foreach ($model->getBonusWallets() as $wallet) {
                $array['bonusWallets'][] = $wallet->getId();
            }
        }

        return $array;
    }

    /**
     * @param Player $model
     * @return bool|mixed
     */
    public function save($model)
    {
        $success = true;

        // [step] store wallets
        if ($success) {
            foreach ($model->retrieveWallets() as $wallet) {
                $this->getWalletsMapper()->save($wallet);
            }
        }

        // [step] store base model
        if ($success) {
            $success = parent::save($model);
        }

        return $success;
    }


    /**
     * @return Wallets
     */
    private function getWalletsMapper()
    {
        return $this->app['mappers.wallets'];
    }

    /**
     * @return Player
     */
    public function createModel()
    {
        return new Player();
    }


}