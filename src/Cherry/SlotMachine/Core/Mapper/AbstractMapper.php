<?php


namespace Cherry\SlotMachine\Core\Mapper;

use Cherry\SlotMachine\Core\AbstractApplication;
use Cherry\SlotMachine\Core\Model\ResourceBaseModelInterface;
use Zend\Hydrator\ClassMethods;

/**
 * Class AbstractMapper
 * @package Cherry\SlotMachine\Core\Mapper
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
abstract class AbstractMapper implements MapperInterface
{
    /**
     * @var AbstractApplication
     */
    protected $app;

    /**
     * @var ClassMethods
     */
    protected $hydrator;

    /**
     * AbstractDBALGateway constructor.
     * @param AbstractApplication $app
     */
    public function __construct($app)
    {
        $this->app = $app;

        // initialize hydrator for extracting model data basing on getters
        $this->hydrator = new ClassMethods(false);
    }

    /** @inheritdoc */
    public function toArray($model)
    {
        return $this->hydrator->extract($model);
    }

    /** @inheritdoc */
    public function populate($model, $data)
    {
        return $this->hydrator->hydrate($data, $model);
    }

    /** @inheritdoc */
    public function populateSingle($model, $field, $value)
    {
        $setter = 'set' . ucfirst($field);

        if (method_exists($model, $setter)) {
            $model->{$setter}($value);
        }

        return $model;
    }

    /** @inheritdoc */
    public function save($model)
    {
        $success = false;
        $data = $this->toArray($model);

        // [case] update
        if ($model->getId()) {
            $success = $this->getGateway()->update($data, $model->getId());
        } // [case] insert
        else {
            $newId = $this->getGateway()->insert($data);

            if (false !== $newId) {
                $model->setId($newId);
                $success = true;
            }
        }

        return $success;
    }

    /** @inheritdoc */
    public function remove($model)
    {
        return $this->getGateway()->remove($model->getId());
    }

    /**
     * @param mixed $id
     * @return ResourceBaseModelInterface|null
     */
    public function findOne($id)
    {
        $model = null;
        $data = $this->getGateway()->findOne($id);

        if (is_array($data)) {
            $model = $this->createModel();

            if (!$model instanceof ResourceBaseModelInterface) {
                throw new \RuntimeException('Mode must be instance of ' . ResourceBaseModelInterface::class);
            }

            $model = $this->populate($model, $data);
        }

        return $model;
    }

    /** @inheritdoc */
    public function find(array $filters = [], array $orderBy = [], $limit = null, $offset = 0)
    {
        $list = [];
        $rawList = $this->getGateway()->find($filters, $orderBy, $limit, $offset);

        foreach ($rawList as $itemData) {
            $model = $this->createModel();
            $model = $this->populate($model, $itemData);

            $list[] = $model;
        }

        return $list;
    }

}