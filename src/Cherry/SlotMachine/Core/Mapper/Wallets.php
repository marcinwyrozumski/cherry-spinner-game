<?php


namespace Cherry\SlotMachine\Core\Mapper;

use Cherry\SlotMachine\Core\Model\Bonus;
use Cherry\SlotMachine\Core\Model\Wallet;
use Cherry\SlotMachine\Core\Gateway\Wallets as WalletsGateway;

class Wallets extends AbstractMapper
{

    /**
     * @return WalletsGateway
     */
    public function getGateway()
    {
        return $this->app['gateways.wallets'];
    }

    /**
     * @return Wallet
     */
    public function createModel()
    {
        return new Wallet();
    }

    /**
     * @param Wallet $model
     * @return array
     */
    public function toArray($model)
    {
        $array = parent::toArray($model);

        if ($model->getAssociatedBonus()) {
            $array['associatedBonus'] = $model->getAssociatedBonus()->getId();
        }

        return $array;
    }


    /** @inheritdoc */
    public function populate($model, $data)
    {
        /** @var Wallet $model */
        $model = parent::populate($model, $data);

        if (!empty($data['associatedBonus'])) {
            /** @var Bonus $bonus */
            $bonus = $this->getBonusMapper()->findOne($data['associatedBonus']);
            $model->setAssociatedBonus($bonus);
        }

        // force update status
        $model->updateStatus();

        return $model;
    }

    /**
     * @param Wallet $model
     * @return bool|mixed
     */
    public function save($model)
    {
        $success = true;

        // [step] store bonus
        if ($model->getAssociatedBonus()) {
            $success = $this->getBonusMapper()->save($model->getAssociatedBonus());
        }

        // [step] store base object
        if ($success) {
            $success = parent::save($model);
        }

        return $success;
    }


    /**
     * @return Bonuses
     */
    private function getBonusMapper()
    {
        return $this->app['mappers.bonuses'];
    }

}