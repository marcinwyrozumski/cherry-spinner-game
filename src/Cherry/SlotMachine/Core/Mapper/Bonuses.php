<?php


namespace Cherry\SlotMachine\Core\Mapper;

use Cherry\SlotMachine\Core\Model\Bonus;
use Cherry\SlotMachine\Core\Gateway\Bonuses as BonusesGateway;
use Cherry\SlotMachine\Core\Model\ResourceBaseModelInterface;
use Cherry\SlotMachine\Core\Model\Reward;

/**
 * Class Bonuses
 * @package Cherry\SlotMachine\Core\Mapper
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Bonuses extends AbstractMapper
{
    /**
     * @return BonusesGateway
     */
    public function getGateway()
    {
        return $this->app['gateways.bonuses'];
    }

    /**
     * @return Bonus
     */
    public function createModel()
    {
        return new Bonus();
    }

    /**
     * @param ResourceBaseModelInterface|Bonus $model
     * @return array
     */
    public function toArray($model)
    {
        $array = parent::toArray($model);

        if ($model->getValueOfReward()) {
            $array['valueOfReward'] = parent::toArray($model->getValueOfReward());
        }


        return $array;
    }


    /**
     * @param ResourceBaseModelInterface|Bonus $model
     * @param array $data
     * @return ResourceBaseModelInterface|Bonus
     */
    public function populate($model, $data)
    {
        /** @var Bonus $model */
        $model = parent::populate($model, $data);

        if (!empty($data['valueOfReward'])) {
            /** @var Reward $reward */
            $reward = parent::populate(new Reward(), $data['valueOfReward']);
            $model->setValueOfReward($reward);
        }

        return $model;
    }


}