<?php


namespace Cherry\SlotMachine\Core\Mapper;
use Cherry\SlotMachine\Core\Gateway\GatewayInterface;
use Cherry\SlotMachine\Core\Model\ResourceBaseModelInterface;

/**
 * Interface MapperInterface
 * @package Cherry\SlotMachine\Core\Mapper
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
interface MapperInterface
{
    /**
     * @return GatewayInterface
     */
    public function getGateway();

    /**
     * @param ResourceBaseModelInterface $model
     * @return array
     */
    public function toArray($model);

    /**
     * @return ResourceBaseModelInterface
     */
    public function createModel();

    /**
     * @param ResourceBaseModelInterface|object $model
     * @param array $data
     * @return ResourceBaseModelInterface|object
     */
    public function populate($model, $data);

    /**
     * @param ResourceBaseModelInterface $model
     * @param string $field
     * @param mixed $value
     * @return ResourceBaseModelInterface
     */
    public function populateSingle($model, $field, $value);

    /**
     * @param ResourceBaseModelInterface $model
     * @return bool
     */
    public function save($model);

    /**
     * @param ResourceBaseModelInterface $model
     * @return bool
     */
    public function remove($model);

    /**
     * @param mixed $id
     * @return ResourceBaseModelInterface
     */
    public function findOne($id);

    /**
     * @param array $filters
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function find(array $filters = [], array $orderBy = [], $limit = null, $offset = 0);
}