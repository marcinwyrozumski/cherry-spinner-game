<?php


namespace Cherry\SlotMachine\Core\Service;

use Cherry\SlotMachine\Core\Mapper\Players as PlayersMapper;

/**
 * Class Players
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Players extends AbstractResourceService
{
    /**
     * @return PlayersMapper
     */
    public function getMapper()
    {
        return $this->app['mappers.players'];
    }

}