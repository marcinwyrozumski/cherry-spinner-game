<?php


namespace Cherry\SlotMachine\Core\Service;

use Cherry\SlotMachine\Core\Model\Money;

/**
 * Class SimpleSpinner
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class SimpleSpinner
{
    const RESULT_JACKPOT = 'jackpot';
    const RESULT_WIN = 'win';
    const RESULT_LOST = 'lost';
    const RESULT_IDLE = 'idle';

    const SPINNER_MIN = 0;
    const SPINNER_MAX = 10;
    const SPINNER_WIN_THRESHOLD = 5;

    /**
     * @var Money
     */
    private $betMoney = 0.0;

    /**
     * @var string
     */
    private $result = SimpleSpinner::RESULT_IDLE;

    /**
     * @var float
     */
    private $winAmount = 0.0;

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return Money
     */
    public function getWinMoney()
    {
        return new Money($this->winAmount, $this->betMoney->getCurrency());
    }


    /**
     * @param Money $betAmount
     * @return $this
     */
    public function setBetMoney(Money $betAmount)
    {
        $this->betMoney = $betAmount;
        return $this;
    }

    /**
     * @return $this
     */
    public function run()
    {
        $this->resetGame();

        // [step] pick random value  for win validation
        $randomNumber = $this->spinNumber();

        // [case] check small win condition
        if ($this->isSpinWin($randomNumber)) {
            $this->result = static::RESULT_WIN;
            $this->monetize($randomNumber);

            // [case] check big win condition
            if ($this->isJackpot($randomNumber)) {
                $this->result = static::RESULT_JACKPOT;
                $this->rewardWithBetAmount();
            }
        }
        // [case] game lost
        else {
            $this->result = static::RESULT_LOST;
        }

        return $this;
    }

    /**
     * Set idle state for game
     */
    private function resetGame()
    {
        $this->winAmount = 0;
        $this->result = static::RESULT_IDLE;
    }

    /**
     * Pick a number for game play
     *
     * @return int
     */
    private function spinNumber()
    {
        return rand(static::SPINNER_MIN, static::SPINNER_MAX);
    }

    /**
     * Check if game spin has been won
     *
     * @return bool
     */
    public function isWin()
    {
        return in_array($this->result, [static::RESULT_WIN, static::RESULT_JACKPOT]);
    }

    /**
     * Checks win condition
     *
     * @param $randomNumber
     * @return bool
     */
    private function isSpinWin($randomNumber)
    {
        return $randomNumber >= static::SPINNER_WIN_THRESHOLD;
    }

    /**
     * Checks big win condition
     *
     * @param $randomNumber
     * @return bool
     */
    private function isJackpot($randomNumber)
    {
        return $randomNumber == static::SPINNER_MAX;
    }

    /**
     * Monetizes win
     *
     * @param $randomNumber
     */
    private function monetize($randomNumber)
    {
        $this->winAmount += (floor($this->betMoney->getAmount()) * $randomNumber) / static::SPINNER_MAX;
    }

    /**
     * Reward
     */
    private function rewardWithBetAmount()
    {
        $this->winAmount += $this->betMoney->getAmount();
    }
}