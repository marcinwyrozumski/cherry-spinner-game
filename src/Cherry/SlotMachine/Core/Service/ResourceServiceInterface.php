<?php


namespace Cherry\SlotMachine\Core\Service;

use Cherry\SlotMachine\Core\Mapper\MapperInterface;
use Cherry\SlotMachine\Core\Model\ResourceBaseModelInterface;

/**
 * Interface ResourceServiceInterface
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
interface ResourceServiceInterface
{
    /**
     * @param array $data
     * @return ResourceBaseModelInterface
     */
    public function create(array $data);

    /**
     * @param mixed $id
     * @return ResourceBaseModelInterface
     */
    public function findOne($id);

    /**
     * @param array $filters
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return ResourceBaseModelInterface[]|array
     */
    public function find(array $filters = [], array $orderBy = [], $limit = null, $offset = 0);

    /**
     * @param array $data
     * @param mixed $id
     * @return ResourceBaseModelInterface
     */
    public function replace(array $data, $id);

    /**
     * @param array $data
     * @param mixed $id
     * @return ResourceBaseModelInterface
     */
    public function update(array $data, $id);

    /**
     * @param mixed $id
     * @return bool
     */
    public function remove($id);

    /**
     * @return MapperInterface
     */
    public function getMapper();

    /**
     * @param ResourceBaseModelInterface $model
     * @return array
     */
    public function toResponseArray($model);
}