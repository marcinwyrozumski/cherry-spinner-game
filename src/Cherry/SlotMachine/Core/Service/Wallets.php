<?php


namespace Cherry\SlotMachine\Core\Service;


/**
 * Class Wallets
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Wallets extends AbstractResourceService
{
    /**
     * @return \Cherry\SlotMachine\Core\Mapper\Wallets
     */
    public function getMapper()
    {
        return $this->app['mappers.wallets'];
    }

}