<?php


namespace Cherry\SlotMachine\Core\Service;

use Cherry\SlotMachine\Core\Mapper\Wallets;
use Cherry\SlotMachine\Core\Model\Bonus;
use Cherry\SlotMachine\Core\Model\Money;
use Cherry\SlotMachine\Core\Model\Player;
use Cherry\SlotMachine\Core\Model\Wallet;
use Cherry\SlotMachine\Core\Pattern\Wallet\Observer\DepositObserver;
use Cherry\SlotMachine\Core\Pattern\Wallet\Observer\LoginObserver;
use Cherry\SlotMachine\Core\Pattern\Wallet\Observer\RealObserver;
use Cherry\SlotMachine\Core\Pattern\Wallet\WalletSubject;

/**
 * Class Deposits
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Deposits extends AbstractService
{

    /**
     * @var Player
     */
    private $player;

    /**
     * @param Player $player
     * @return $this
     */
    public function forPlayer(Player $player)
    {
        $this->player = $player;
        return $this;
    }

    /**
     * Trigger observer on deposit money
     *
     * @param Money $money
     */
    public function deposit(Money $money)
    {
        $this->onTrigger(Bonus::TRIGGER_DEPOSIT, $money);
    }

    /**
     * Trigger observer on login
     */
    public function login()
    {
        $this->onTrigger(Bonus::TRIGGER_LOGIN);
    }

    /**
     * @param Wallet $wallet
     * @param string $trigger
     * @param Money|null $money
     * @return WalletSubject
     */
    protected function initializeSubject(Wallet $wallet, $trigger, Money $money = null)
    {
        $walletSubject = new WalletSubject($wallet, $trigger, $money);

        $walletSubject->attach(new RealObserver());
        $walletSubject->attach(new DepositObserver());
        $walletSubject->attach(new LoginObserver());

        return $walletSubject;
    }

    /**
     * @return Wallets
     */
    private function getWalletsMapper()
    {
        return $this->app['mappers.wallets'];
    }

    /**
     * @param $trigger
     * @param Money $money
     */
    protected function onTrigger($trigger, Money $money = null)
    {
        $wallets = $this->player->retrieveWallets();

        foreach ($wallets as $wallet) {
            $walletSubject = $this->initializeSubject($wallet, $trigger, $money);
            $walletSubject->notify();

            if ($walletSubject->isIsNotified()) {
                $this->getWalletsMapper()->save($wallet);
            }
        }
    }
}