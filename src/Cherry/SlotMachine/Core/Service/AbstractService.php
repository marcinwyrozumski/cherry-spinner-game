<?php


namespace Cherry\SlotMachine\Core\Service;


use Cherry\SlotMachine\Core\AbstractApplication;

/**
 * Class AbstractService
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
abstract class AbstractService
{
    /**
     * @var AbstractApplication
     */
    protected $app;

    /**
     * WagingRequirements constructor.
     * @param AbstractApplication $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }
}