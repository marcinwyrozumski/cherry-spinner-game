<?php


namespace Cherry\SlotMachine\Core\Service;

use Cherry\SlotMachine\Core\Mapper\Bonuses as BonusesMapper;

/**
 * Class Bonuses
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Bonuses extends AbstractResourceService
{
    /**
     * @return BonusesMapper
     */
    public function getMapper()
    {
        return $this->app['mappers.bonuses'];
    }

}