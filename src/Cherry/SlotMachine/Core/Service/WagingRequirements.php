<?php


namespace Cherry\SlotMachine\Core\Service;

use Cherry\SlotMachine\Core\Mapper\Wallets;
use Cherry\SlotMachine\Core\Model\Bonus;
use Cherry\SlotMachine\Core\Model\Money;
use Cherry\SlotMachine\Core\Model\Player;
use Cherry\SlotMachine\Core\Model\Wallet;

/**
 * Class WagingRequirements
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class WagingRequirements extends AbstractService
{
    /**
     * @param Player $player
     * @param Money $betAmount
     * @return bool
     */
    public function canWage(Player $player, Money $betAmount)
    {
        $out = false;

        // [step] check if player has any balance at all
        // [step] check player's balance is at least equal to bet amount
        if ($player->calculateBalance() >= $betAmount->getAmount()) {
            $out = true;
        }

        return $out;
    }

    /**
     * @param Player $player
     * @param Money $betAmount
     * @return bool
     */
    public function wage(Player $player, Money $betAmount)
    {
        $waged = false;
        $proceed = true;
        $wallet = $player->getRealWallet();

        // [case] wage real money first
        if ($proceed && $this->isActive($wallet)) {
            $waged = true;
            $betAmount = $this->removeMoneyToBottomLevel($wallet, $betAmount);
            $this->getWalletsMapper()->save($wallet);

            // [case] all bet money removed from real wallet
            if ($betAmount->getAmount() == 0) {
               $proceed = false;
            }
        }

        // [case] wage bonus money when real money is depleted
        if ($proceed) {
            foreach ($player->getBonusWallets() as $wallet) {
                if ($this->isActive($wallet)) {
                    $waged = true;
                    $betAmount = $this->removeMoneyToBottomLevel($wallet, $betAmount);
                    $this->getWalletsMapper()->save($wallet);

                    if ($betAmount->getAmount() == 0) {
                        break;
                    }
                }
            }
        }

        return $waged;
    }


    /**
     * @param Player $player
     * @param Money $money
     * @return bool
     */
    public function monetizeGame(Player $player, Money $money)
    {
        $proceed = true;

        // [step] monetize bonus wallets
        if ($proceed) {
            foreach ($player->getBonusWallets() as $wallet) {

                // [case] checks monetize requirements
                if ($this->shouldMonetize($wallet)) {
                    $money = $this->addMoneyToWagedLevel($wallet, $money);

                    $this->getWalletsMapper()->save($wallet);

                    // [case] if there are any leftover money from win
                    // don't break the loop and increment another bonus
                    if ($money->getAmount() == 0) {
                        $proceed = false;
                        break;
                    }
                }
            }
        }

        // [step] monetize real wallet if bonus are wagered
        if ($proceed) {
            $realWallet = $player->getRealWallet();
            $realWallet->addMoney($money);

            $this->getWalletsMapper()->save($realWallet);
        }

        return $this;
    }

    /**
     * If wallet's bonus has active state this means wagering requirements are filled
     *
     * @param Wallet $wallet
     * @return bool
     */
    protected function shouldMonetize(Wallet $wallet)
    {
        $out = Bonus::STATUS_ACTIVE !== $wallet->getAssociatedBonus()->getStatus();

        return $out;
    }

    /**
     * @param Wallet $wallet
     * @param Money $money
     * @return Money
     */
    protected function addMoneyToWagedLevel(Wallet $wallet, Money $money)
    {
        $bonus = $wallet->getAssociatedBonus();

        if (!$bonus) {
            throw new \RuntimeException('Only wallets with assigned bonus can be used');
        }

        // get minimum required amount for filling cash in waging requirements
        $wagedLevelAmount = $wallet->calculateCashInWageringRequirements();

        // [case] if wallet already met cash in req, return money
        if ($wallet->getAmount() >= $wagedLevelAmount) {
            $leftOverMoney = $money;
        } // [case] calculate leftover money
        else {
            // calculate  leftover money that's left after meeting minimum cash in requirements
            $leftOverAmount = ($wallet->getAmount() + $money->getAmount()) - $wagedLevelAmount;

            // only positive money is supported
            if ($leftOverAmount < 0) {
                $leftOverAmount = 0;
            }

            // add money to wallet to meeting wagering requirements level
            $wallet->addMoney(new Money(($money->getAmount() - $leftOverAmount), $money->getCurrency()));

            // leftover money should be added to another wallet, thus it should be returned
            $leftOverMoney = new Money($leftOverAmount, $money->getCurrency());
        }

        return $leftOverMoney;
    }

    /**
     * @param Wallet $wallet
     * @param Money $money
     * @return Money
     */
    protected function removeMoneyToBottomLevel(Wallet $wallet, Money $money)
    {
        $leftOverWalletAmount = $wallet->getAmount() - $money->getAmount();

        // [case] when bet money exceeds wallet value
        if ($leftOverWalletAmount < 0) {
            $removeMoneyAmount = $money->getAmount() - abs($leftOverWalletAmount);
            $removeMoney = new Money($removeMoneyAmount, $money->getCurrency());
            $outMoneyAmount = abs($leftOverWalletAmount);
        }
        // [case] when wallet had enough money
        else {
            $removeMoney = $money;
            $outMoneyAmount = 0;
        }

        $wallet->removeMoney($removeMoney);
        $outMoney = new Money($outMoneyAmount, $money->getCurrency());

        return $outMoney;
    }

    /**
     * @param Wallet $wallet
     * @return bool
     */
    protected function isActive(Wallet $wallet)
    {
        return $wallet->getStatus() == Wallet::STATUS_ACTIVE;
    }

    /**
     * @return Wallets
     */
    protected function getWalletsMapper()
    {
        return $this->app['mappers.wallets'];
    }

}