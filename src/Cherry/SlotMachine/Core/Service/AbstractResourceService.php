<?php


namespace Cherry\SlotMachine\Core\Service;

use Cherry\SlotMachine\Core\Mapper\MapperInterface;
use Cherry\SlotMachine\Core\Model\ResourceBaseModelInterface;

/**
 * Class AbstractResourceService
 * @package Cherry\SlotMachine\Core\Service
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
abstract class AbstractResourceService extends AbstractService implements ResourceServiceInterface
{

    /**
     * @param array $data
     * @return bool|ResourceBaseModelInterface
     */
    public function create(array $data)
    {
        $newModel = $this->getMapper()->createModel();

        if (!($newModel instanceof ResourceBaseModelInterface)) {
            throw new \RuntimeException('Model must be instance of ' . ResourceBaseModelInterface::class);
        }

        $newModel = $this->getMapper()->populate($newModel, $data);
        $success = $this->getMapper()->save($newModel);

        if ($success) {
            return $newModel;
        }

        return false;
    }

    /**
     * @param mixed $id
     * @return ResourceBaseModelInterface
     */
    public function findOne($id)
    {
        return $this->getMapper()->findOne($id);
    }

    /**
     * @param array $filters
     * @param array $orderBy
     * @param null $limit
     * @param int $offset
     * @return ResourceBaseModelInterface[]
     */
    public function find(array $filters = [], array $orderBy = [], $limit = null, $offset = 0)
    {
        return $this->getMapper()->find($filters, $orderBy, $limit, $offset);
    }

    /**
     * @param array $data
     * @param mixed $id
     * @return bool|ResourceBaseModelInterface
     */
    public function replace(array $data, $id)
    {
        $model = $this->getMapper()->findOne($id);

        if ($model) {
            $model = $this->getMapper()->populate($model, $data);
            $success = $this->getMapper()->save($model);

            if ($success) {
                return $model;
            }
        }

        return false;
    }

    /**
     * @param array $data
     * @param mixed $id
     * @return bool|ResourceBaseModelInterface
     */
    public function update(array $data, $id)
    {
        $model = $this->getMapper()->findOne($id);

        if ($model) {
            foreach ($data as $key => $value) {
                $this->getMapper()->populateSingle($model, $key, $value);
            }

            $success = $this->getMapper()->save($model);

            if ($success) {
                return $model;
            }
        }

        return false;
    }

    /**
     * @param mixed $id
     * @return bool
     */
    public function remove($id)
    {
        $model = $this->getMapper()->findOne($id);

        if ($model) {
            return $this->getMapper()->remove($model);
        }

        return false;
    }

    /**
     * @return MapperInterface
     */
    abstract public function getMapper();

    /**
     * @param ResourceBaseModelInterface $model
     * @return array
     */
    public function toResponseArray($model)
    {
        return $this->getMapper()->toArray($model);
    }

}