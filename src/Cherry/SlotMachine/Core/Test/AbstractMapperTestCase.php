<?php


namespace Cherry\SlotMachine\Core\Test;


use Cherry\SlotMachine\UI\Application;
use Cherry\SlotMachine\Core\Mapper\AbstractMapper;

abstract class AbstractMapperTestCase extends AbstractTestCase
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var AbstractMapper
     */
    protected $mapper;

    protected function setUp()
    {
        $this->app = new Application();

        $mapperServiceName = $this->getMapperServiceName();

        /** @var AbstractMapper mapper */
        $this->mapper = $this->app[$mapperServiceName];
    }

    /**
     * @dataProvider getPopulateProvider()
     * @param array $testData
     */
    public function testPopulate($testData)
    {
        $model = $this->mapper->createModel();

        $this->mapper->populate($model, $testData);

        $this->assertPopulate($model, $testData);

    }

    /**
     * @dataProvider getPopulateProvider()
     *
     * @param array $testData
     */
    public function testToArray($testData)
    {
        $testModel = $this->mapper->createModel();
        $this->mapper->populate($testModel, $testData);


        $array = $this->mapper->toArray($testModel);
        unset($array['id']);

        foreach ($this->getIgnoredFields() as $field) {
            unset($array[$field]);
            unset($testData[$field]);
        }

        ksort($array);
        ksort($testData);

        assertThat($array, is(equalTo($testData)));

    }

    /**
     * @return array
     */
    protected function getIgnoredFields()
    {
        return [];
    }

    abstract protected function assertPopulate($model, $testData);

    /**
     * @return string
     */
    abstract protected function getMapperServiceName();

    /**
     * @return array
     */
    abstract public function getPopulateProvider();
}