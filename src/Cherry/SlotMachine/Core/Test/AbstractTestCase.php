<?php


namespace Cherry\SlotMachine\Core\Test;

use Cherry\SlotMachine\Core\AbstractApplication;
use Hamcrest\MatcherAssert;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class AbstractTestCase
 * @package R2CEE\Test
 * @author Marcin Wyrozumski <marcin.wyrozumski@mail.com>
 */
abstract class AbstractTestCase extends PHPUnit_Framework_TestCase
{
    
    /**
     * Collects assertions performed by Hamcrest matchers during the test.
     *
     * @throws \Exception
     * @throws null
     */
    public function runBare()
    {
        MatcherAssert::resetCount();
        $e = null;

        try {
            parent::runBare();
        } catch (\Exception $e) {

        }

        $count = MatcherAssert::getCount();
        $this->addToAssertionCount($count);

        if (isset($e)) {
            throw $e;
        }
    }

    /**
     * Tears down Mockery
     */
    protected function tearDown()
    {
        Mockery::close();
    }
}