<?php


namespace Cherry\SlotMachine\Core\Controller;


use Cherry\SlotMachine\Core\AbstractApplication;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractController
 * @package Cherry\SlotMachine\Core
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
abstract class AbstractController
{

    /**
     * @var AbstractApplication
     */
    protected $app;

    /**
     * IndexController constructor.
     * @param AbstractApplication $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @param array $data
     * @param int $statusCode
     * @return JsonResponse
     */
    protected function createJsonResponse(array $data, $statusCode=Response::HTTP_OK)
    {
        $response =  new JsonResponse($data, $statusCode);

        return $response;
    }

    /**
     * @param array $dataOut
     * @param string $status
     * @param int $statusCode
     * @return array
     */
    protected function prepareResponseData(array $dataOut, $status = 'ok', $statusCode = Response::HTTP_OK)
    {
        $result = [
            'data' => $dataOut,
            'meta' => [
                'status' => $status,
                'code' => $statusCode
            ]
        ];

        return $result;
    }
}