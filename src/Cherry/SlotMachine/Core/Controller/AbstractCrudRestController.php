<?php


namespace Cherry\SlotMachine\Core\Controller;

use Cherry\SlotMachine\Core\Service\ResourceServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractCrudController
 * @package Cherry\SlotMachine\Core\Controller
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
abstract class AbstractCrudRestController extends AbstractController
{
    /**
     * @return ResourceServiceInterface
     */
    abstract protected function getResourceService();

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $dataOut = [];
        $filters = $request->get('filters', []);
        $orderBy = $request->get('orderBy', []);
        $limit = $request->get('limit', 10);
        $offset = $request->get('offset', 0);

        $list = $this->getResourceService()->find($filters, $orderBy, $limit, $offset);

        foreach ($list as $model) {
            $dataOut[] = $this->getResourceService()->toResponseArray($model);
        }

        $result = $this->prepareResponseData($dataOut);

        $response = $this->createJsonResponse($result);

        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $result = [];
        $statusCode = Response::HTTP_CREATED;
        $dataIn = $request->request->all();

        // @todo add validation

        if (!empty($dataIn)) {

            $model = $this->getResourceService()->create($dataIn);

            if (!$model) {
                throw new \RuntimeException("New resource could not be created");
            }

            $dataOut = $this->getResourceService()->toResponseArray($model);
            $result = $this->prepareResponseData($dataOut);

        } else {
            $statusCode = Response::HTTP_BAD_REQUEST;
        }

        $response = $this->createJsonResponse($result, $statusCode);

        return $response;
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function getAction(Request $request, $id)
    {
        $statusCode = Response::HTTP_OK;
        $result = [];

        $model = $this->getResourceService()->findOne($id);

        if ($model) {
            $dataOut = $this->getResourceService()->toResponseArray($model);
            $result = $this->prepareResponseData($dataOut);

        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        $response = $this->createJsonResponse($result, $statusCode);

        return $response;
    }

    /**
     * @param Request $request
     * @param mixed $id
     * @return JsonResponse
     */
    public function replaceAction(Request $request, $id)
    {
        $statusCode = Response::HTTP_OK;
        $result = [];

        $dataIn = $request->request->all();

        // @todo add validation

        if (!$dataIn) {
            throw new \InvalidArgumentException('Empty data');
        }

        $model = $this->getResourceService()->findOne($id);

        if ($model) {
            $updatedModel = $this->getResourceService()->replace($dataIn, $id);

            if ($updatedModel) {
                $dataOut = $this->getResourceService()->toResponseArray($updatedModel);
                $result = $this->prepareResponseData($dataOut, 'replaced');
            } else {
                throw new \RuntimeException("Resource could not be replaced");
            }

        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        $response = $this->createJsonResponse($result, $statusCode);

        return $response;
    }

    /**
     * @param Request $request
     * @param mixed $id
     * @return JsonResponse
     */
    public function updateAction(Request $request, $id)
    {
        $statusCode = Response::HTTP_OK;
        $result = [];

        $dataIn = $request->request->all();

        // @todo add validation

        $model = $this->getResourceService()->findOne($id);

        if ($model) {
            $updatedModel = $this->getResourceService()->update($dataIn, $id);

            if ($updatedModel) {
                $dataOut = $this->getResourceService()->toResponseArray($updatedModel);
                $result = $this->prepareResponseData($dataOut, 'updated');
            } else {
                throw new \RuntimeException("Resource could not be replaced");
            }

        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        $response = $this->createJsonResponse($result, $statusCode);

        return $response;

    }

    /**
     * @param Request $request
     * @param mixed $id
     * @return JsonResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $statusCode = Response::HTTP_OK;
        $result = [];
        $model = $this->getResourceService()->findOne($id);

        if ($model) {
            if (!$this->getResourceService()->remove($id)) {
                throw  new \RuntimeException('Resource could not be deleted');
            }

            $result = [
                'status' => 'deleted',
                'id' => $id
            ];
        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        $response = $this->createJsonResponse($result, $statusCode);

        return $response;
    }
}