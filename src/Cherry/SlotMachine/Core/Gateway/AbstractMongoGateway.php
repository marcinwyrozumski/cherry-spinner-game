<?php


namespace Cherry\SlotMachine\Core\Gateway;

use Cherry\SlotMachine\Core\AbstractApplication;

/**
 * Class AbstractMongoLGateway
 * @package Cherry\SlotMachine\Core\Gateway
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
abstract class AbstractMongoGateway implements GatewayInterface
{
    /**
     * @var AbstractApplication
     */
    protected $app;

    /**
     * @var \MongoClient
     */
    protected $connection;

    /**
     * AbstractDBALGateway constructor.
     * @param AbstractApplication $app
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->initializeConnection();
    }

    /** @inheritdoc */
    public function remove($id)
    {
        $criteria = [
            $this->getPrimaryKeyName() => $id
        ];

        return (bool)$this->selectCollection()->remove($criteria);
    }

    /** @inheritdoc */
    public function insert(array $data)
    {
        // ensures primary index
        $this->selectCollection()->createIndex([$this->getPrimaryKeyName() => 1]);

        $result = $this->selectCollection()->insert($data);

        return $this->isSuccess($result);
    }

    /** @inheritdoc */
    public function update(array $data, $id)
    {
        $criteria = [
            $this->getPrimaryKeyName() => $id
        ];

        $result = $this->selectCollection()->update($criteria, $data, ['upsert' => true]);

        return $this->isSuccess($result);
    }

    /** @inheritdoc */
    public function findOne($id)
    {
        $criteria = [
            $this->getPrimaryKeyName() => $id
        ];

        $item = $this->selectCollection()->findOne($criteria);

        return $item;
    }

    /** @inheritdoc */
    public function find(array $filters = [], array $orderBy = [], $limit = null, $offset = 0)
    {
        $list = [];

        // @todo add common filtering pattern
        $cursor = $this->selectCollection()->find($filters);

        if ($orderBy) {
            $cursor->sort($orderBy);
        }
        if (is_int($limit)) {
            $cursor->limit($limit);
        }
        if ($offset) {
            $cursor->skip($offset);
        }

        while ($cursor->hasNext()) {
            $list[] = $cursor->getNext();
        }

        return $list;
    }

    /**
     * @return string
     */
    abstract public function getDatabaseName();

    /**
     * @return string
     */
    abstract public function getCollectionName();

    /**
     * Setting standard _id as primary
     *
     * @inheritdoc
     */
    public function getPrimaryKeyName()
    {
        return 'id';
    }

    /**
     * @return mixed
     */
    private function getConfiguration()
    {
        return $this->app->getConfig()['cherry.slotMachine.mongodb'];
    }

    /**
     * @return \MongoCollection
     */
    public function selectCollection()
    {
        $collection = $this->connection->selectCollection(
            $this->getDatabaseName(), $this->getCollectionName()
        );

        return $collection;
    }


    /**
     *  Initializes connection
     */
    private function initializeConnection()
    {
        $connectionConfig = $this->getConfiguration();

        $this->connection = new \MongoClient(
            sprintf("mongodb://%s:%s", $connectionConfig['host'], $connectionConfig['port']),
            $connectionConfig['options'],
            $connectionConfig['driverOptions']
        );
    }

    /**
     * @param $result
     * @return bool
     */
    private function isSuccess($result)
    {
        if ($result['err']) {
            throw new \RuntimeException($result['err']);
        }

        return $result['ok'] == 1;
    }


}