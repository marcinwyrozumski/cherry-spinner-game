<?php


namespace Cherry\SlotMachine\Core\Gateway;

/**
 * Class Wallets
 * @package Cherry\SlotMachine\Core\Gateway
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Wallets extends AbstractMongoGateway
{
    /** @inheritdoc */
    public function getDatabaseName()
    {
        return 'slot-machine';
    }

    /** @inheritdoc */
    public function getCollectionName()
    {
        return 'wallets';
    }
}