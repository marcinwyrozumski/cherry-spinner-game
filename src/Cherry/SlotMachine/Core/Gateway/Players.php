<?php


namespace Cherry\SlotMachine\Core\Gateway;

/**
 * Class Players
 * @package Cherry\SlotMachine\Core\Gateway
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Players extends AbstractMongoGateway
{
    /** @inheritdoc */
    public function getDatabaseName()
    {
        return 'slot-machine';
    }

    /** @inheritdoc */
    public function getCollectionName()
    {
        return 'players';
    }

}