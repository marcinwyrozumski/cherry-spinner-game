<?php


namespace Cherry\SlotMachine\Core\Gateway;

/**
 * Class CommonDBAL
 * @package Cherry\SlotMachine\Core\Gateway
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Bonuses extends AbstractMongoGateway
{
    /** @inheritdoc */
    public function getDatabaseName()
    {
        return 'slot-machine';
    }

    /** @inheritdoc */
    public function getCollectionName()
    {
        return 'bonuses';
    }
    
}