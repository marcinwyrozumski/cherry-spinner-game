<?php


namespace Cherry\SlotMachine\Core\Gateway;
use Cherry\SlotMachine\Core\Model\ResourceBaseModelInterface;

/**
 * Interface GatewayInterface
 * @package Cherry\SlotMachine\Core\Gateway
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
interface GatewayInterface
{
    /**
     * @return string
     */
    public function getPrimaryKeyName();

    /**
     * @param array $data
     * @return bool|mixed
     */
    public function insert(array $data);

    /**
     * @param array $data
     * @param mixed $id
     * @return bool|mixed
     */
    public function update(array $data, $id);

    /**
     * @param mixed $id
     * @return bool
     */
    public function remove($id);

    /**
     * @param mixed $id
     * @return array
     */
    public function findOne($id);

    /**
     * @param array $filters
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function find(array $filters = [], array $orderBy = [], $limit = null, $offset = 0);

}