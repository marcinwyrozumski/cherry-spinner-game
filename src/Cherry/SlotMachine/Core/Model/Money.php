<?php


namespace Cherry\SlotMachine\Core\Model;

/**
 * Class Money
 * @package Cherry\SlotMachine\Model
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Money
{

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var string
     */
    protected $currency = null;

    /**
     * Money constructor.
     * @param $amount
     * @param $currency
     */
    public function __construct($amount, $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return (float) $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    public function __toString()
    {
        return number_format($this->getAmount(), 2, ', ', ' ') . " " . $this->getCurrency();
    }


}