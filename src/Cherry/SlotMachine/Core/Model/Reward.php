<?php


namespace Cherry\SlotMachine\Core\Model;
use Cherry\SlotMachine\Core\Shared\Currency;

/**
 * Class Reward
 * @package Cherry\SlotMachine\Model
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Reward
{

    const TYPE_PERCENTAGE = 'percentage';
    const TYPE_MONEY = 'money';

    /**
     * @var number
     */
    protected $reward;

    /**
     * @var string
     */
    protected $type;

    /**
     * @return number
     */
    public function getReward()
    {
        return (float)$this->reward;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param number $reward
     * @return $this
     */
    public function setReward($reward)
    {
        if (!is_numeric($reward)) {
            throw new \InvalidArgumentException("Only numeric rewards allowed for percentage");
        }

        $this->reward = $reward;
        return $this;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        if (static::TYPE_PERCENTAGE != $type && static::TYPE_MONEY != $type) {
            throw new \RuntimeException("Unsupported type of reward: '{$type}'");
        }

        $this->type = $type;
        return $this;
    }


    /**
     * @param Money $money
     * @return Money
     */
    public function calculateReward($money)
    {
        $calculated = 0.0;

        switch ($this->type) {
            case static::TYPE_MONEY:
                $calculated = $this->getReward();
                break;

            case static::TYPE_PERCENTAGE:
                if ($money) {
                    $calculated = floor(($this->getReward() * $money->getAmount()) / 100);
                }
                break;
        }

        return new Money($calculated, $money ? $money->getCurrency() : Currency::EUR);
    }

}