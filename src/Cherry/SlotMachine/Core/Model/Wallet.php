<?php


namespace Cherry\SlotMachine\Core\Model;

use Cherry\SlotMachine\Core\Shared\WalletInterface;

/**
 * Class Wallet
 * @package Cherry\SlotMachine\Model
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Wallet implements ResourceBaseModelInterface, WalletInterface
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var float
     */
    protected $amount = 0.0;

    /**
     * @var string
     */
    protected $currency = "EUR";

    /**
     * @var int
     */
    protected $initialValue = 0;

    /**
     * @var string
     */
    protected $status = WalletInterface::STATUS_DEPLETED;

    /**
     * @var string
     */
    protected $createdDate = null;

    /**
     * @var string
     */
    protected $modifiedDate = null;

    /**
     * @var string
     */
    protected $origin = WalletInterface::ORIGIN_DEFAULT;

    /**
     * @var Bonus
     */
    protected $associatedBonus = null;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Bonus
     */
    public function getAssociatedBonus()
    {
        return $this->associatedBonus;
    }

    /**
     * @param Bonus $associatedBonus
     * @return $this
     */
    public function setAssociatedBonus($associatedBonus)
    {
        $this->associatedBonus = $associatedBonus;
        return $this;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }


    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getInitialValue()
    {
        return $this->initialValue;
    }

    /**
     * @param int $initialValue
     * @return $this
     */
    public function setInitialValue($initialValue)
    {
        $this->initialValue = $initialValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param string $createdDate
     * @return $this
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * @param string $modifiedDate
     * @return $this
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function calculateCashInWageringRequirements()
    {
        $out = 0;
        if ($this->getAssociatedBonus()) {
            $out = $this->getInitialValue() * $this->getAssociatedBonus()->getWageringMultiplier();
        }

        return $out;
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function addMoney(Money $money)
    {
        $added = false;

        if ($this->currency == $money->getCurrency()) {
            $this->amount += $money->getAmount();
            $this->updateStatus();
            $added = true;
        }

        return $added;
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function removeMoney(Money $money)
    {
        $removed = false;

        if ($this->currency == $money->getCurrency()) {
            $this->amount -= $money->getAmount();
            $this->updateStatus();
            $removed = true;
        }

        return $removed;
    }

    /**
     * @return $this
     */
    public function updateStatus()
    {
        // [case] for bonus wallets
        if ($this->getAssociatedBonus()) {
            $bonus = $this->getAssociatedBonus();

            // [case] wallet is depleted
            if ($this->getAmount() <= 0) {
                $this->setStatus(Wallet::STATUS_DEPLETED);
                $bonus->setStatus(Bonus::STATUS_DEPLETED);
            }
            // [case] wallet meets cash in req and it is active, REAL MONEY!!!!
            else if ($this->getAmount() >= $this->calculateCashInWageringRequirements()) {
                $this->setStatus(Wallet::STATUS_ACTIVE);
                $bonus->setStatus(Bonus::STATUS_ACTIVE);
            }
            // [case] wallet in wagering state (bonus not real money in wallet)
            else {
                $this->setStatus(Wallet::STATUS_ACTIVE);
                $bonus->setStatus(Bonus::STATUS_WAGERED);
            }
        }
        // [case] for real wallets
        else {
            // [case] wallet
            if ($this->getAmount() > 0) {
                $this->setStatus(Wallet::STATUS_ACTIVE);
            } else {
                $this->setStatus(Wallet::STATUS_DEPLETED);
            }
        }

        return $this;
    }

}