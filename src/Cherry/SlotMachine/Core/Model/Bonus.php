<?php


namespace Cherry\SlotMachine\Core\Model;


class Bonus implements ResourceBaseModelInterface
{
    const STATUS_ACTIVE = 'active';
    const STATUS_WAGERED = 'wagered';
    const STATUS_DEPLETED = 'depleted';

    const TRIGGER_LOGIN = 'login';
    const TRIGGER_DEPOSIT = 'deposit';

    /**
     * @var
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $trigger;

    /**
     * @var Reward
     */
    protected $valueOfReward;

    /**
     * @var int
     */
    protected $wageringMultiplier;

    /**
     * @var string
     */
    protected $status = Bonus::STATUS_WAGERED;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTrigger()
    {
        return $this->trigger;
    }

    /**
     * @param string $trigger
     * @return $this
     */
    public function setTrigger($trigger)
    {
        $this->trigger = $trigger;
        return $this;
    }

    /**
     * @return Reward
     */
    public function getValueOfReward()
    {
        return $this->valueOfReward;
    }

    /**
     * @param Reward $valueOfReward
     * @return $this
     */
    public function setValueOfReward($valueOfReward)
    {
        $this->valueOfReward = $valueOfReward;
        return $this;
    }

    /**
     * @return int
     */
    public function getWageringMultiplier()
    {
        return $this->wageringMultiplier;
    }

    /**
     * @param int $wageringMultiplier
     * @return $this
     */
    public function setWageringMultiplier($wageringMultiplier)
    {
        $this->wageringMultiplier = $wageringMultiplier;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }


}