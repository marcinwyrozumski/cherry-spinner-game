<?php


namespace Cherry\SlotMachine\Core\Model;

/**
 * Class Player
 * @package Cherry\SlotMachine\Model
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Player implements ResourceBaseModelInterface
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var int;
     */
    protected $age;

    /**
     * @var string
     */
    protected $gender;

    /**
     * @var Wallet
     */
    protected $realWallet;

    /**
     * @var Wallet[]
     */
    protected $bonusWallets = [];

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = md5($password);
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return $this
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return Wallet
     */
    public function getRealWallet()
    {
        return $this->realWallet;
    }

    /**
     * @param Wallet $realWallet
     * @return $this
     */
    public function setRealWallet($realWallet)
    {
        $this->realWallet = $realWallet;
        return $this;
    }

    /**
     * @return Wallet[]
     */
    public function getBonusWallets()
    {
        return $this->bonusWallets;
    }

    /**
     * @param Wallet[] $bonusWallets
     * @return $this
     */
    public function setBonusWallets($bonusWallets)
    {
        $this->bonusWallets = $bonusWallets;
        return $this;
    }

    /**
     * @param Wallet $wallet
     * @return $this
     */
    public function addRealWallet(Wallet $wallet)
    {
        $this->realWallet[] = $wallet;
        return $this;
    }

    /**
     * @param Wallet $wallet
     * @return $this
     */
    public function addBonusWallet(Wallet $wallet)
    {
        $this->bonusWallets[] = $wallet;
        return $this;
    }

    /**
     * @return float
     */
    public function calculateBalance()
    {
        $amount = 0.0;
        $wallets = $this->retrieveWallets();

        /** @var Wallet $wallet */
        foreach ($wallets as $wallet) {
            $amount += $wallet->getAmount();
        }

        return $amount;
    }

    /**
     * @return float
     */
    public function calculateRealBalance()
    {
        $amount = $this->realWallet->getAmount();

        foreach ($this->getBonusWallets() as $wallet) {
            if (
                Bonus::STATUS_ACTIVE === $wallet->getAssociatedBonus()->getStatus() &&
                Wallet::STATUS_ACTIVE === $wallet->getStatus()
            ) {
                $amount += $wallet->getAmount();
            }
        }

        return $amount;
    }

    /**
     * @return Wallet[]
     */
    public function retrieveWallets()
    {
        return array_merge([$this->realWallet], $this->bonusWallets);
    }
}