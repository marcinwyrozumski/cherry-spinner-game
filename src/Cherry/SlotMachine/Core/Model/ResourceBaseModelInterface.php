<?php


namespace Cherry\SlotMachine\Core\Model;

/**
 * Interface ModelBaseInterface
 * @package Cherry\SlotMachine\Core\Model
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
interface ResourceBaseModelInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id);
}