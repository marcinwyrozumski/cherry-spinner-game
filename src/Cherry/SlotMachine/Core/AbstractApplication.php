<?php


namespace Cherry\SlotMachine\Core;

use Cherry\SlotMachine\Core\Service\Bonuses as BonusesService;
use Cherry\SlotMachine\Core\Mapper\Bonuses as BonusesMapper;
use Cherry\SlotMachine\Core\Gateway\Bonuses as BonusesGateway;
use Cherry\SlotMachine\Core\Service\Players as PlayersService;
use Cherry\SlotMachine\Core\Mapper\Players as PlayersMapper;
use Cherry\SlotMachine\Core\Gateway\Players as PlayersGateway;
use Cherry\SlotMachine\Core\Service\Wallets as WalletsService;
use Cherry\SlotMachine\Core\Mapper\Wallets as WalletsMapper;
use Cherry\SlotMachine\Core\Gateway\Wallets as WalletsGateway;
use Silex\Application as SilexApplication;
use Silex\Application\UrlGeneratorTrait;
use Silex\Provider\ServiceControllerServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AbstractApplication
 * @package Cherry\SlotMachine\Core
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
abstract class AbstractApplication extends SilexApplication
{
    use UrlGeneratorTrait;

    /**
     * @var array
     */
    protected $controllers = [];

    /**
     * Application constructor.
     * @param array $values
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->initConfig();

        $this->initServiceProviders();
        $this->initServices();
        $this->iniControllers();
        $this->initRoutes();
        $this->initMiddleware();
    }

    private function initConfig()
    {
        $this['config'] = Yaml::parse(file_get_contents('../config/env-prod.yml'));
    }

    protected function initServiceProviders()
    {
        $this->register(new ServiceControllerServiceProvider());
    }

    protected function initServices()
    {
        $this['services.players'] = function () {
            return new PlayersService($this);
        };

        $this['mappers.players'] = function () {
            return new PlayersMapper($this);
        };

        $this['gateways.players'] = function () {
            return new PlayersGateway($this);
        };

        $this['services.bonuses'] = function () {
            return new BonusesService($this);
        };

        $this['mappers.bonuses'] = function () {
            return new BonusesMapper($this);
        };

        $this['gateways.bonuses'] = function () {
            return new BonusesGateway($this);
        };

        $this['services.wallets'] = function () {
            return new WalletsService($this);
        };

        $this['mappers.wallets'] = function () {
            return new WalletsMapper($this);
        };

        $this['gateways.wallets'] = function () {
            return new WalletsGateway($this);
        };
    }

    protected function iniControllers()
    {
        foreach ($this->controllers as $controllerName) {
            $controllerClass = $this->getCalledNamespace() . '\\Controller\\' . ucfirst($controllerName);
            $this['controllers.' . $controllerName] = function () use ($controllerName, $controllerClass) {
                $controllerClass = new $controllerClass($this);
                return $controllerClass;
            };

        }
    }

    protected function initRoutes()
    {
    }

    protected function initMiddleware()
    {
        // [step] utilize json content type
        $this->before(function (Request $request) {
            if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                $data = json_decode($request->getContent(), true);
                $request->request->replace(is_array($data) ? $data : []);
            }
        });

        // [step] allow cors
        $this->after(function (Request $request, Response $response) {
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set("Access-Control-Allow-Methods","GET,POST,PUT,DELETE");
        });
    }


    /**
     * @return array
     */
    public function getConfig()
    {
        return $this['config'];
    }

    /**
     * @return string
     */
    abstract protected function getCalledNamespace();

    /**
     * Creates crud routes
     *
     * @param string $path
     * @param string $controller
     */
    protected function createCrud($path, $controller)
    {

        $this->get($path, "controllers.{$controller}Crud:indexAction")
            ->bind("get_{$controller}_list");

        $this->post($path, "controllers.{$controller}Crud:createAction")
            ->bind("create_{$controller}");

        $this->get("{$path}/{id}", "controllers.{$controller}Crud:getAction")
            ->convert('id', function ($id) { return (int) $id; })
            ->bind("get_{$controller}");

        $this->put("{$path}/{id}", "controllers.{$controller}Crud:replaceAction")
            ->convert('id', function ($id) { return (int) $id; })
            ->bind("replace_{$controller}");

        $this->patch("{$path}/{id}", "controllers.{$controller}Crud:updateAction")
            ->convert('id', function ($id) { return (int) $id; })
            ->bind("update_{$controller}");

        $this->delete("{$path}/{id}", "controllers.{$controller}Crud:deleteAction")
            ->convert('id', function ($id) { return (int) $id; })
            ->bind("delete_{$controller}");
    }

}