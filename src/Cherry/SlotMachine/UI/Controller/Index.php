<?php

namespace Cherry\SlotMachine\UI\Controller;

use Cherry\SlotMachine\Core\Controller\AbstractController;
use Cherry\SlotMachine\Core\Service\Players;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class IndexController
 * @package Cherry\SlotMachine\Controller
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Index extends AbstractController
{

    /**
     * @return string
     */
    public function indexAction()
    {
        return $this->app['twig']->render('slot-machine/index.twig');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function simulationAction(Request $request)
    {
        $playerId = (int)$request->get('playerId');
        $players = $this->getPlayersService()->find();

        if ($playerId) {
            $player = $this->getPlayersService()->findOne($playerId);
        } else {
            $player = current($players);
        }

        $urlBase = $this->app->getConfig()['cherry.slotMachine.api']['url'];

        return $this->app['twig']->render('slot-machine/simulate.twig', [
            'players' => $players,
            'player' => $player,
            'api' => [
                'playUrl' => $urlBase . '/simulate/play',
                'depositUrl' => $urlBase . '/simulate/deposit',
                'loginUrl' => $urlBase . '/simulate/login',
            ],
            'refreshUrl' => '/playerData'
        ]);
    }

    /**
     * @param Request $request
     * @param $playerId
     * @return mixed
     */
    public function getPlayerDetailsAction(Request $request, $playerId)
    {
        $player = $this->getPlayersService()->findOne($playerId);

        return $this->app['twig']->render('slot-machine/playerData.twig', [
            'player' => $player
        ]);
    }

    /**
     * @return Players
     */
    private function getPlayersService()
    {
        /** @var Players $players */
        $players = $this->app['services.players'];
        return $players;
    }


}