<?php


namespace Cherry\SlotMachine\UI;

use Cherry\SlotMachine\Core\AbstractApplication;
use Silex\Application as SilexApplication;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;

/**
 * Class Application
 * @package Cherry\SlotMachine
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Application extends AbstractApplication
{
    /**
     * @var array
     */
    protected $controllers = [
        'index',
    ];

    protected function initServiceProviders()
    {
        parent::initServiceProviders();

        $this->register(new TwigServiceProvider(), [
            'twig.path' => realpath($this->getConfig()['cherry.slotMachine.templates']['path']),
        ]);

        $this->register(new FormServiceProvider());
        $this->register(new LocaleServiceProvider());
        $this->register(new TranslationServiceProvider(), [
            'translator.domains' => [],
        ]);
    }

    protected function initRoutes()
    {
        $this->get("/", "controllers.index:indexAction")
            ->bind('index_page');

        $this->get("/simulate", "controllers.index:simulationAction")
            ->bind('index_simulation');

        $this->get("/playerData/{playerId}", "controllers.index:getPlayerDetailsAction")
            ->bind('index_player_data')
            ->assert('playerId', '\d+')
            ->convert('playerId', function ($playerId) { return (int) $playerId; });;

    }


    /**
     * @return string
     */
    protected function getCalledNamespace()
    {
        return __NAMESPACE__;
    }


}