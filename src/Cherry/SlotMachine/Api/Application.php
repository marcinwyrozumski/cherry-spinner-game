<?php


namespace Cherry\SlotMachine\Api;

use Cherry\SlotMachine\Core\AbstractApplication;
use Cherry\SlotMachine\Core\Service\Deposits as DepositsService;
use Cherry\SlotMachine\Core\Service\SimpleSpinner;
use Cherry\SlotMachine\Core\Service\WagingRequirements;
use Silex\Application as SilexApplication;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Application
 * @package Cherry\SlotMachine
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Application extends AbstractApplication
{
    /**
     * @var array
     */
    protected $controllers = [
        'bonusesCrud', 'walletsCrud', 'playersCrud', 'simulation'
    ];

    protected function initRoutes()
    {
        $this->post('/api/v1/simulate/login', 'controllers.simulation:loginAction')
            ->bind('simulate_login');
        
        $this->post('/api/v1/simulate/deposit', 'controllers.simulation:depositAction')
            ->bind('simulate_deposit');

        $this->post('/api/v1/simulate/play', 'controllers.simulation:playAction')
            ->bind('simulate_play');

        $this->createCrud("/api/v1/wallets", "wallets");

        $this->createCrud("/api/v1/bonuses", "bonuses");

        $this->createCrud("/api/v1/players", "players");
    }

    protected function initServices()
    {
        parent::initServices();

        $this['services.gameSpinner'] = function () {
            return new SimpleSpinner($this);
        };

        $this['services.wagingRequirements'] = function () {
            return new WagingRequirements($this);
        };

        $this['services.deposits'] = function () {
            return new DepositsService($this);
        };
    }


    protected function initMiddleware()
    {
        parent::initMiddleware();

        $this->error(function (\Exception $e, Request $request, $code) {
            // handle error codes
            switch ($code) {
                case 404:
                    $message = 'The requested page could not be found.';
                    break;
                default:
                    $message = 'We are sorry, but something went terribly wrong.';
            }

            $data = [
                'message' => $message
            ];

            // add debuging trace
            if ($this['debug']) {
                $data['exception'] = [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ];
            }


            return new JsonResponse([
                'meta' => [
                    'code' => $code,
                    'status' => 'error'
                ],
                'data' => $data
            ]);
        });
    }


    /**
     * @return string
     */
    protected function getCalledNamespace()
    {
        return __NAMESPACE__;
    }


}