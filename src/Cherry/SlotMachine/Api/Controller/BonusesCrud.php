<?php


namespace Cherry\SlotMachine\Api\Controller;

use Cherry\SlotMachine\Core\Controller\AbstractCrudRestController;
use Cherry\SlotMachine\Core\Service\Bonuses;

/**
 * Class BonusCrud
 * @package Cherry\SlotMachine\Controller
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class BonusesCrud extends AbstractCrudRestController
{
    /**
     * @return Bonuses
     */
    protected function getResourceService()
    {
        return $this->app['services.bonuses'];
    }

}