<?php


namespace Cherry\SlotMachine\Api\Controller;

use Cherry\SlotMachine\Core\Controller\AbstractCrudRestController;
use Cherry\SlotMachine\Core\Service\Wallets;

/**
 * Class Wallets
 * @package Cherry\SlotMachine\Api\Controller
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class WalletsCrud extends AbstractCrudRestController
{
    /**
     * @return Wallets
     */
    protected function getResourceService()
    {
        return $this->app['services.wallets'];
    }

}