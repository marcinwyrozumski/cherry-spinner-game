<?php


namespace Cherry\SlotMachine\Api\Controller;


use Cherry\SlotMachine\Core\Controller\AbstractController;
use Cherry\SlotMachine\Core\Model\Money;
use Cherry\SlotMachine\Core\Model\Player;
use Cherry\SlotMachine\Core\Service\Deposits;
use Cherry\SlotMachine\Core\Service\Players;
use Cherry\SlotMachine\Core\Service\SimpleSpinner;
use Cherry\SlotMachine\Core\Service\WagingRequirements;
use Cherry\SlotMachine\Core\Shared\Currency;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Simulation
 * @package Cherry\SlotMachine\Api\Controller
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class Simulation extends AbstractController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $proceed = true;
        $message = null;
        $player = null;

        $playerId = (int)$request->get('playerId');

        // [step] get player
        if ($proceed) {
            $player = $this->getPlayersService()->findOne($playerId);

            if (!($player instanceof Player)) {
                $proceed = false;
                $message = 'Invalid player supplied';
            }
        }

        // [step] deposit money
        if ($proceed) {
            $this->getDepositsService()->forPlayer($player)->login();
        }

        // [step] prepare failure response
        return new JsonResponse([
            'message' => $message
        ], $message ? 400 : 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function depositAction(Request $request)
    {
        $deposited = false;
        $proceed = true;
        $message = null;
        $player = null;

        $playerId = (int)$request->get('playerId');
        $depositValue = (float)$request->get('depositAmount', 0.0);

        // [step] get player
        if ($proceed) {
            $player = $this->getPlayersService()->findOne($playerId);

            if (!($player instanceof Player)) {
                $proceed = false;
                $message = 'Invalid player supplied';
            }
        }

        // [step] deposit money
        if ($proceed) {
            $deposited = true;
            $depositMoney = new Money($depositValue, Currency::EUR);
            $this->getDepositsService()->forPlayer($player)->deposit($depositMoney);
        }

        // [step] prepare failure response
        return new JsonResponse([
            'deposited' => $deposited,
            'message' => $message
        ], $message ? 400 : 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function playAction(Request $request)
    {
        $proceed = true;
        $player = null;
        $message = null;

        $playerId = (int)$request->get('playerId');
        $betAmount = (float)$request->get('betAmount', 0.0);
        $betMoney = new Money($betAmount, Currency::EUR);

        // [step] get player
        if ($proceed) {
            $player = $this->getPlayersService()->findOne($playerId);

            if (!($player instanceof Player)) {
                $proceed = false;
                $message = 'Invalid player supplied';
            }
        }

        // [step] check betAmount
        if ($proceed && $betAmount <= 0) {
            $proceed = false;
            $message = 'Insufficient betAmount';
        }

        // [step] check if waging is allowed for user
        if ($proceed && !$this->getWagingService()->canWage($player, $betMoney)) {
            $proceed = false;
            $message = 'Player has insufficient balance to bet';
        }

        // [step] wage bet
        if ($proceed) {
            // [step] place bet
            $waged = $this->getWagingService()->wage($player, $betMoney);

            if (!$waged) {
                $proceed = false;
                $message = 'Bet could not be waged';
            }
        }

        // [step] issue game
        if ($proceed) {
            // [step] run game
            $this
                ->getGame()
                ->setBetMoney($betMoney)
                ->run();

            // [step] monetize
            if ($this->getGame()->isWin()) {
                $this->getWagingService()->monetizeGame($player, $this->getGame()->getWinMoney());
            }

            return new JsonResponse([
                'result' => $this->getGame()->getResult(),
                'winAmount' => [
                    'amount' => $this->getGame()->getWinMoney()->getAmount(),
                    'currency' => $this->getGame()->getWinMoney()->getCurrency()
                ]
            ]);
        }

        // [step] prepare failure response
        return new JsonResponse(['message' => $message], 400);
    }

    /**
     * @return WagingRequirements
     */
    private function getWagingService()
    {
        /** @var WagingRequirements $waging */
        $waging = $this->app['services.wagingRequirements'];
        return $waging;
    }

    /**
     * @return SimpleSpinner
     */
    private function getGame()
    {
        /** @var SimpleSpinner $game */
        $game = $this->app['services.gameSpinner'];
        return $game;
    }

    /**
     * @return Players
     */
    private function getPlayersService()
    {
        /** @var Players $players */
        $players = $this->app['services.players'];
        return $players;
    }

    /**
     * @return Deposits
     */
    private function getDepositsService()
    {
        /** @var Deposits $deposits */
        $deposits = $this->app['services.deposits'];
        return $deposits;
    }
}