<?php


namespace Cherry\SlotMachine\Api\Controller;

use Cherry\SlotMachine\Core\Controller\AbstractCrudRestController;
use Cherry\SlotMachine\Core\Service\Players;

/**
 * Class PlayerCrud
 * @package Cherry\SlotMachine\Controller
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class PlayersCrud extends AbstractCrudRestController
{
    /**
     * @return Players
     */
    protected function getResourceService()
    {
        return $this->app['services.players'];
    }

}