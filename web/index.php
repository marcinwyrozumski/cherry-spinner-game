<?php

$filename = __DIR__.preg_replace('#(\?.*)$#', '', $_SERVER['REQUEST_URI']);
if (php_sapi_name() === 'cli-server' && is_file($filename)) {
    return false;
}

ini_set('display_errors', 1);
date_default_timezone_set('Europe/Warsaw');

require_once  __DIR__ . "/../app/setupApp.php";


$app->run();
