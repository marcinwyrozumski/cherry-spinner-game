import requests
import os
import json
from pprint import pprint

class ResourceRestore:

    def __init__(self):
        self.baseUrl = "http://localhost:8081"

        self.headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }

    def run(self):
        self.createResource('bonuses.json', '/api/v1/bonuses')
        self.createResource('wallets.json', '/api/v1/wallets')
        self.createResource('players.json', '/api/v1/players')

    def getDir(self):
        return os.path.dirname(os.path.realpath(__file__))

    def createResource(self, filename, api_url):
        print("=========================================================================================")
        print ('Creates: ' + filename)

        list = json.loads(open(self.getDir() + '/../data/' + filename).read())

        for payload in list:
            response = requests.request("POST", self.baseUrl + api_url, data=json.dumps(payload), headers=self.headers)
            pprint(response)

        print("=========================================================================================")


app = ResourceRestore()
app.run()