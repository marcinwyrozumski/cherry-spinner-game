<?php

require_once __DIR__ . '/../vendor/autoload.php';


$app = new \Cherry\SlotMachine\UI\Application();

$app['debug'] = true;

return $app;