<?php

require_once __DIR__ . '/../vendor/autoload.php';

$app = new \Cherry\SlotMachine\Api\Application();

$app['debug'] = true;

return $app;