<?php


namespace CherryTest\SlotMachine\Core\Mapper;

use Cherry\SlotMachine\Core\Model\Bonus;
use Cherry\SlotMachine\Core\Model\Reward;
use Cherry\SlotMachine\Core\Test\AbstractMapperTestCase;

/**
 * Class BonusesTest
 * @package CherryTest\SlotMachine\Core\Mapper
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class BonusesTest extends AbstractMapperTestCase
{

    /** @inheritdoc */
    protected function getMapperServiceName()
    {
        return 'mappers.bonuses';
    }

    /**
     * @param Bonus $model
     * @param array $testData
     */
    protected function assertPopulate($model, $testData)
    {
        assertThat($model->getName(), is(equalTo($testData['name'])));
        assertThat($model->getStatus(), is(equalTo($testData['status'])));
        assertThat($model->getTrigger(), is(equalTo($testData['trigger'])));
        assertThat($model->getWageringMultiplier(), is(equalTo($testData['wageringMultiplier'])));

        assertThat($model->getValueOfReward(), is(anInstanceOf(Reward::class)));

        assertThat($model->getValueOfReward()->getReward(), is(equalTo($testData['valueOfReward']['reward'])));
        assertThat($model->getValueOfReward()->getType(), is(equalTo($testData['valueOfReward']['type'])));
    }


    /**
     * @return array
     */
    public function getPopulateProvider()
    {
        return [
            [
                [
                    'name' => 'Bonus 1',
                    'status' => Bonus::STATUS_ACTIVE,
                    'trigger' => Bonus::TRIGGER_DEPOSIT,
                    'wageringMultiplier' => 21,
                    'valueOfReward' => [
                        'reward' => 30,
                        'type' => Reward::TYPE_MONEY
                    ]
                ]
            ]
        ];
    }
}