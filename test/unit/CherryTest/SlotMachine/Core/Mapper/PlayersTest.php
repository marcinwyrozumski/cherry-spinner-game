<?php


namespace CherryTest\SlotMachine\Core\Mapper;


use Cherry\SlotMachine\Core\Model\Player;
use Cherry\SlotMachine\Core\Model\Wallet;
use Cherry\SlotMachine\Core\Service\Wallets;
use Cherry\SlotMachine\Core\Test\AbstractMapperTestCase;

class PlayersTest extends AbstractMapperTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->app['mappers.wallets'] = function () {
            return $this->createWalletsMapperMock();
        };
    }

    /**
     * @param Player $model
     * @param array $testData
     */
    protected function assertPopulate($model, $testData)
    {
        assertThat($model->getUsername(), is(equalTo($testData['username'])));
        assertThat($model->getPassword(), is(equalTo(md5($testData['password']))));
        assertThat($model->getName(), is(equalTo($testData['name'])));
        assertThat($model->getLastName(), is(equalTo($testData['lastName'])));
        assertThat($model->getAge(), is(equalTo($testData['age'])));
        assertThat($model->getGender(), is(equalTo($testData['gender'])));

        assertThat($model->getRealWallet(), is(anInstanceOf(Wallet::class)));
        assertThat($model->getRealWallet()->getId(), is(equalTo($testData['realWallet'])));

        foreach ($model->getBonusWallets() as $key => $wallet) {
            assertThat($wallet, is(anInstanceOf(Wallet::class)));
            assertThat($wallet->getId(), is(equalTo($testData['bonusWallets'][$key])));
        }
    }

    /**
     * @return string
     */
    protected function getMapperServiceName()
    {
        return 'mappers.players';
    }

    /** @inheritdoc */
    public function getPopulateProvider()
    {
        return [
            [
                [
                    'username' => 'user',
                    'password' => 'abce',
                    'name' => 'John',
                    'lastName' => 'Doe',
                    'age' => 30,
                    'gender' => Player::GENDER_MALE,
                    'realWallet' => 1,
                    'bonusWallets' => [2, 3]
                ]
            ]
        ];
    }

    /**
     * @return \Mockery\MockInterface
     */
    protected function createWalletsMapperMock()
    {
        $mock = \Mockery::mock(Wallets::class);

        for ($i = 1; $i <= 3; $i++) {
            $wallet = new Wallet();
            $wallet->setId($i);
            $mock->shouldReceive('findOne')->withArgs([$i])->andReturn($wallet);

        }

        return $mock;
    }

    /** @inheritdoc */
    protected function getIgnoredFields()
    {
        return [
            'password'
        ];
    }


}