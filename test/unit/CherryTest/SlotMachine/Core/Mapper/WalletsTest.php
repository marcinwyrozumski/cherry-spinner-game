<?php


namespace CherryTest\SlotMachine\Core\Mapper;

use Cherry\SlotMachine\Core\Mapper\Bonuses;
use Cherry\SlotMachine\Core\Mapper\Wallets;
use Cherry\SlotMachine\Core\Model\Bonus;
use Cherry\SlotMachine\Core\Model\Wallet;
use Cherry\SlotMachine\Core\Test\AbstractMapperTestCase;

/**
 * Class WalletsTest
 * @package CherryTest\SlotMachine\Core\Mapper
 * @author Marcin Wyrozumski <marcin.wyrozumski@gmail.com>
 */
class WalletsTest extends AbstractMapperTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->app['mappers.bonuses'] = function() {
            return $this->createBonusesMapperMock();
        };
    }

    /**
     * @param Wallet $model
     * @param array $testData
     */
    protected function assertPopulate($model, $testData)
    {
        assertThat($model->getAmount(), is(equalTo($testData['amount'])));
        assertThat($model->getCurrency(), is(equalTo($testData['currency'])));
        assertThat($model->getInitialValue(), is(equalTo($testData['initialValue'])));
        assertThat($model->getStatus(), is(equalTo($testData['status'])));
        assertThat($model->getCreatedDate(), is(equalTo($testData['createdDate'])));
        assertThat($model->getModifiedDate(), is(equalTo($testData['modifiedDate'])));
        assertThat($model->getOrigin(), is(equalTo($testData['origin'])));

        assertThat($model->getAssociatedBonus(), is(anInstanceOf(Bonus::class)));
    }


    /**
     * @return string
     */
    protected function getMapperServiceName()
    {
        return 'mappers.wallets';
    }

    /** @inheritdoc */
    public function getPopulateProvider()
    {
        return [
            [
                [
                    'amount' => 3000.0,
                    'currency' => 'EUR',
                    'initialValue' => 4300,
                    'status' => Wallet::STATUS_ACTIVE,
                    'createdDate' => '2015-12-01',
                    'modifiedDate' => '2015-12-02',
                    'origin' => Wallet::ORIGIN_DEFAULT,
                    'associatedBonus' => 1
                ]
            ]
        ];
    }

    /**
     * @return \Mockery\MockInterface
     */
    protected function createBonusesMapperMock()
    {
        $mock = \Mockery::mock(Bonuses::class);

        $bonus = new Bonus();
        $bonus->setId(1);
        $mock->shouldReceive('findOne')->withArgs([1])->andReturn($bonus);

        return $mock;
    }
}