<?php

/**
 * Bootstrap Configuration file for Relocator unit tests
 */

require_once __DIR__ . '/../../vendor/autoload.php';

require_once  __DIR__ .'/../../vendor/hamcrest/hamcrest-php/hamcrest/Hamcrest.php';

// setting base directory for unit tests
chdir(__DIR__ . DIRECTORY_SEPARATOR .'..' );

