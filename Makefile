all: build api ui

build:
	php composer.phar install

api:
	docker build -t api ./docker/api

ui:
	docker build -t ui ./docker/ui

.PHONY: build api all
